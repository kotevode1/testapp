//
//  ContentView.swift
//  TestApp
//
//  Created by Mark on 15.07.2021.
//

import SwiftUI
import AppTrackingTransparency

struct ContentView: View {
    var body: some View {
        Text("hello")
            .padding()
            .onAppear {
                ATTrackingManager.requestTrackingAuthorization { status in
                    debugPrint(status)
                }
            }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
