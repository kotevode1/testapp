//
//  TestAppApp.swift
//  TestApp
//
//  Created by Mark on 15.07.2021.
//

import SwiftUI

@main
struct TestAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
